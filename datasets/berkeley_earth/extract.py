import sys

def extract(block_name):
	print 'Extracting %s' % (block_name)
	data_section_found = False
	start_found = False
	end_found = False
	
	output = ''
	
	f = open('Land_and_Ocean_LatLong1.csv')

	for line in f:
		if not data_section_found and 'data:' in line:
			data_section_found = True
		if data_section_found and not start_found and block_name + ' =' in line:
			start_found = True
			line = line.replace('%s = ' % (block_name), '')
		if data_section_found and start_found and ';' in line and start_found:
			end_found = True
			line = line.replace(';', '')
		if start_found:
			output = output + '\n'.join([_.strip() for _ in line.strip().split(',')])
		if end_found:
			break

	f.close()

	with open(block_name, 'w') as f:
		f.write(output)

	print 'Extracted %s to file named %s' % (block_name, block_name)

if __name__ == '__main__':
	if len(sys.argv) >= 2:
		extract(sys.argv[1])
	else:
		print 'Usage is python extract.py column_name'
