import sys
import math
import struct

def get_min_max(fname):
	_min = float('Inf')
	_max = -float('Inf')
	
	f = open(fname)
	for line in f:
		num = float(line.strip())
		if _min > num:
			_min = num
		elif _max < num:
			_max = num

	f.close()

	return _min, _max

def create_buckets(fname, _min, _max, cardinality):
	# Compute the range of the funtion
	_range = abs(_min - _max)
	print 'range: %f' % (_range)

	# Compute the difference (aka increment) between consecutive buckets
	_inc = (abs(_min) + abs(_max)) / cardinality
	print 'inc: %f' % (_inc)

	# Create a hashmap with buckets labeled from 0 to cardinality - 1
	# The value of the bucket is the middle value for the bucket's range
	# Example: 2.5 is the value [0, 5] 
	_buckets = {}
        _key = 0
	for _ in range(0, cardinality):
		_buckets[_key] = _min + _inc * (_ + 0.5)
		_key = _key + 1

	print 'buckets: ',
	print _buckets

	f = open(fname)
	output_f = open(fname + '_cardinality_' + str(cardinality), 'wb')

	# Write the mid-value the number of times each value in the bucket
	# appears in the column
	for line in f:
		num = float(line.strip())
		_key = math.floor((num - _min) / _inc)
		# In case the num is the maximum value
		if _key == cardinality:
			_key = _key - 1
		C = _buckets[_key]
		output_f.write(struct.pack('i', C))
	
	f.close()
	output_f.close()

if __name__ == '__main__':
	if len(sys.argv) >= 3:
		fname = sys.argv[1]
		cardinality = int(sys.argv[2].strip())
		_min, _max = get_min_max(fname)
		print 'min: %f, max: %f' % (_min, _max)
		create_buckets(fname, _min, _max, cardinality)
	else:
		print 'Usage is python generate.py name_of_attribute cardinality'		
