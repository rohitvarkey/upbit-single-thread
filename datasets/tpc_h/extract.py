import os
import sys

##
# The column name and column number can be found at tpc.org/tpc_documents_current_versions/pdf/tpc-h_v2.17.1.pdf - Page 13
# Quantity is the attribtue required for the simulation - it's 4th in the column
##
def extract(column_name, column_num):
	print 'Extracting %s from column %d' % (column_name, column_num)
	path = 'tpch_2_17_0/ref_data/100/'
	lineitem_files = [path + _ for _ in os.listdir(path) if 'lineitem' in _]

	output = ''
	
	for f_name in lineitem_files:
		f = open(f_name)
		for line in f:
			column = line.split('|')[column_num]
			output = output + column.strip() + '\n'
	
	with open(column_name, 'w') as f:
		f.write(output)
	
	print 'Extracted %s into file called %s' % (column_name, column_name)
	

if __name__ == '__main__':
	if len(sys.argv) >= 3:
		extract(sys.argv[1], int(sys.argv[2]))
	else:
		print 'Usage is python extract.py column_name column_num'
