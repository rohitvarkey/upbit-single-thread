#!/usr/bin/python
import sys
import matplotlib.pyplot as plt

def plot_thread_test(ext):
	x = []
	y = []
	with open('results/thread_test_' + ext, 'r') as f:
		lines = f.readlines()

	for line in lines:
		tokens = line.split(',')
		x.append(int(tokens[0]))
		y.append(int(tokens[1]))

	print x, y

	plt.bar(x, y, color='blue')
	plt.ylabel('Time in us')
	plt.xlabel('Number of threads')
	plt.title('100000 queries with 50000 updates on multithreaded UpBit')
	plt.show()

def plot_appr_test(ext):
	x = range(0, 15, 3) + range(25, 40, 3) + range(50, 65, 3)
	xticks = []
	y = []
	with open('results/approach_test_' + ext, 'r') as f:
		lines = f.readlines()

	newlines = [0, 1, 2, 1, 0] * 3
	ctr = 0
	for line in lines:
		tokens = line.split(',')
		xtick = tokens[0] + '-' + tokens[1] + ' (' + tokens[2] + '%)'
		for _ in range(0, newlines[ctr]):
			xtick = '\n' + xtick
		ctr = ctr + 1
		xticks.append(xtick)
		y.append(int(tokens[3]))

	plt.xticks(x, xticks)
	plt.bar(x, y, color='blue')
	plt.ylabel('Time in us')
	plt.xlabel('Update mix')
	plt.title('10000 queries with x% updates on inplace, UCB and UpBit')
	plt.show()

def get_median(temp):
	sorted(temp)
	return temp[len(temp) / 2]

def build_median_list(f):
	median_f = []
	for idx in range(0, 100, 20):
		temp = f[idx:idx+20]
		median_f.append(get_median(temp))
	return median_f

def plot_fence_length_test(ext):
	x = [1, 2, 4, 8, 16]
	f_0 = []
	f_250 = []
	f_500 = []
	f_1000 = []
	f_1250 = []
	with open('results/fence_length_test_' + ext, 'r') as f:
		lines = f.readlines()

	for line in lines:
		tokens = line.split(',')
		if tokens[0] == '0':
			f_0.append(int(tokens[2]))
		elif tokens[0] == '250':
			f_250.append(int(tokens[2]))
		elif tokens[0] == '500':
			f_500.append(int(tokens[2]))
		elif tokens[0] == '1000':
			f_1000.append(int(tokens[2]))
		elif tokens[0] == '1250':
			f_1250.append(int(tokens[2]))

	temp = []
	median_f_0 = build_median_list(f_0)
	median_f_250 = build_median_list(f_250)
	median_f_500 = build_median_list(f_500)
	median_f_1000 = build_median_list(f_1000)
	# median_f_1250 = build_median_list(f_1250)

	plt.plot(x, median_f_0, label='len(FP) = 0')
	plt.plot(x, median_f_250, label='len(FP) = 250')
	plt.plot(x, median_f_500, label='len(FP) = 500')
	plt.plot(x, median_f_1000, label='len(FP) = 1000')
	# plt.plot(x, median_f_1250, label='len(FP) = 1250')
	plt.legend()

	plt.ylabel('Time in us')
	plt.xlabel('Number of threads')
	plt.title('Performance with respect to fence pointer length')
	plt.show()

if __name__ == '__main__':
	if len(sys.argv) < 2:
		print 'Usage is plot.py name_of_data_binary'
	else:
		plot_thread_test(sys.argv[1])
		plot_appr_test(sys.argv[1])
		plot_fence_length_test(sys.argv[1])
