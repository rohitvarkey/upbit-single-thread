#!/bin/bash

# Initialize required paths
UPBIT=$HOME/GT_MSCS/CS6400/project/upbit-single-thread
RESULTS=$UPBIT/demo/results

# Usage is upbit_thread_test $APPROACH $MODE $NUM_ROWS $CARDINALITY $IDX_PATH $NUM_QUERIES $REMOVED $NUM_THREADS $VERBOSE
function upbit_thread_test {
		TOTAL_TIME_TAKEN=`$UPBIT/build/upbit --approach $1 --mode $2 --num-rows $3 --cardinality $4 --index-path $5 --num-queries $6 --removed $7 --num-threads $8 --verbose $9 | tee /dev/tty | grep 'total time taken'`
		printf "\nRan the binary with the following values approach: $1, mode: $2, number of rows: $3, cardinality: $4, path to index: $5, number of queries: $6, removed rows: $7, number of threads: $8, verbose: $9\n"
		Y_AXIS=`echo $TOTAL_TIME_TAKEN | cut -d ':' -f2 | cut -d ' ' -f2` # Output contains number of threads as X-Axis and time taken as Y-Axis
		printf "\nThe total time taken was $Y_AXIS us\n"
		echo $8,$Y_AXIS
}

# Usage is upbit_appr_test $APPROACH $MODE $NUM_ROWS $CARDINALITY $IDX_PATH $NUM_QUERIES $REMOVED $NUM_THREADS $VERBOSE
function upbit_appr_test {
	TOTAL_TIME_TAKEN=`$UPBIT/build/upbit --approach $1 --mode $2 --num-rows $3 --cardinality $4 --index-path $5 --num-queries $6 --removed $7 --num-threads $8 --verbose $9 | tee /dev/tty | grep 'total time taken'`
	printf "\nRan the binary with the following values approach: $1, mode: $2, number of rows: $3, cardinality: $4, path to index: $5, number of queries: $6, removed rows: $7, number of threads: $8, verbose: $9\n"
	Y_AXIS=`echo $TOTAL_TIME_TAKEN | cut -d ':' -f2 | cut -d ' ' -f2` # Output contains number of threads as X-Axis and time taken as Y-Axis
	printf "\nThe total time taken was $Y_AXIS us\n"
	RATIO=$(( (($7 * 100) / ($6 + $7)) + 1 ))
	echo $1,$8,$RATIO,$Y_AXIS
}

# Usage is for i in $(seq 1 10); do upbit_fence_test $APPROACH $MODE $NUM_ROWS $CARDINALITY $IDX_PATH $NUM_QUERIES $REMOVED $NUM_THREADS $FENCE_LENGTH $VERBOSE
function upbit_fence_test {
	TOTAL_TIME_TAKEN=`$UPBIT/build/upbit --approach $1 --mode $2 --num-rows $3 --cardinality $4 --index-path $5 --num-queries $6 --removed $7 --num-threads $8 --fence-length $9 --verbose ${10} | tee /dev/tty | grep 'total time taken'`
	printf "\nRan the binary with the following values approach: $1, mode: $2, number of rows: $3, cardinality: $4, path to index: $5, number of queries: $6, removed rows: $7, number of threads: $8, fence length: $9 verbose: ${10}\n"
	Y_AXIS=`echo $TOTAL_TIME_TAKEN | cut -d ':' -f2 | cut -d ' ' -f2` # Output contains number of threads as X-Axis and time taken as Y-Axis
	printf "\nThe total time taken was $Y_AXIS us\n"
	echo $9,$8,$Y_AXIS
}

##
# Test runners
##
function run_thread_test {
	echo $1
	THREAD_TEST_RESULTS=$RESULTS/thread_test_$1
	rm $THREAD_TEST_RESULTS
	# Store only the last line which contains the result
	upbit_thread_test ub update $2 4 ${UPBIT}/${1} 100000 100000 1 0 | tee /dev/tty | tail -n1 >> $THREAD_TEST_RESULTS
	upbit_thread_test ub update $2 4 ${UPBIT}/${1} 100000 100000 2 0 | tee /dev/tty | tail -n1 >> $THREAD_TEST_RESULTS
	upbit_thread_test ub update $2 4 ${UPBIT}/${1} 100000 100000 4 0 | tee /dev/tty | tail -n1 >> $THREAD_TEST_RESULTS
	upbit_thread_test ub update $2 4 ${UPBIT}/${1} 100000 100000 8 0 | tee /dev/tty | tail -n1 >> $THREAD_TEST_RESULTS
	upbit_thread_test ub update $2 4 ${UPBIT}/${1} 100000 100000 16 0 | tee /dev/tty | tail -n1 >> $THREAD_TEST_RESULTS
}

function run_appr_test {
	APPR_TEST_RESULTS=$RESULTS/approach_test_$1
	rm $APPR_TEST_RESULTS
	# Store only the last line which contains the result
	# 1% updates
	upbit_appr_test inplace update $2 4 ${UPBIT}/${1} 10000 100 1 0 | tee /dev/tty | tail -n1 >> $APPR_TEST_RESULTS
	upbit_appr_test inplace update $2 4 ${UPBIT}/${1} 10000 100 4 0 | tee /dev/tty | tail -n1 >> $APPR_TEST_RESULTS
	upbit_appr_test ucb update $2 4 ${UPBIT}/${1} 10000 100 1 0 | tee /dev/tty | tail -n1 >> $APPR_TEST_RESULTS
	upbit_appr_test ub update $2 4 ${UPBIT}/${1} 10000 100 1 0 | tee /dev/tty | tail -n1 >> $APPR_TEST_RESULTS
	upbit_appr_test ub update $2 4 ${UPBIT}/${1} 10000 100 4 0 | tee /dev/tty | tail -n1 >> $APPR_TEST_RESULTS

	# 5% updates
	upbit_appr_test inplace update $2 4 ${UPBIT}/${1} 10000 500 1 0 | tee /dev/tty | tail -n1 >> $APPR_TEST_RESULTS
	upbit_appr_test inplace update $2 4 ${UPBIT}/${1} 10000 500 4 0 | tee /dev/tty | tail -n1 >> $APPR_TEST_RESULTS
	upbit_appr_test ucb update $2 4 ${UPBIT}/${1} 10000 500 1 0 | tee /dev/tty | tail -n1 >> $APPR_TEST_RESULTS
	upbit_appr_test ub update $2 4 ${UPBIT}/${1} 10000 500 1 0 | tee /dev/tty | tail -n1 >> $APPR_TEST_RESULTS
	upbit_appr_test ub update $2 4 ${UPBIT}/${1} 10000 500 4 0 | tee /dev/tty | tail -n1 >> $APPR_TEST_RESULTS

	# 10% updates
	upbit_appr_test inplace update $2 4 ${UPBIT}/${1} 10000 1000 1 0 | tee /dev/tty | tail -n1 >> $APPR_TEST_RESULTS
	upbit_appr_test inplace update $2 4 ${UPBIT}/${1} 10000 1000 4 0 | tee /dev/tty | tail -n1 >> $APPR_TEST_RESULTS
	upbit_appr_test ucb update $2 4 ${UPBIT}/${1} 10000 1000 1 0 | tee /dev/tty | tail -n1 >> $APPR_TEST_RESULTS
	upbit_appr_test ub update $2 4 ${UPBIT}/${1} 10000 1000 1 0 | tee /dev/tty | tail -n1 >> $APPR_TEST_RESULTS
	upbit_appr_test ub update $2 4 ${UPBIT}/${1} 10000 1000 4 0 | tee /dev/tty | tail -n1 >> $APPR_TEST_RESULTS
}

function run_fence_length_test {
	FL_TEST_RESULTS=$RESULTS/fence_length_test_$1
	rm $FL_TEST_RESULTS
	# Store only the last line which contains the result
	# No fence pointers

	for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 1000 1 0 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done
	for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 1000 2 0 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done
	for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 1000 4 0 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done
	for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 1000 8 0 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done
	for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 1000 16 0 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done

	# Fence pointer of length 250
	for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 1000 1 250 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done
	for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 1000 2 250 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done
	for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 1000 4 250 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done
	for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 1000 8 250 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done
	for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 1000 16 250 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done

	# Fence pointer of length 500
	for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 1000 1 500 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done
	for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 1000 2 500 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done
	for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 1000 4 500 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done
	for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 1000 8 500 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done
	for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 1000 16 500 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done

	# Fence pointer of length 1000
	for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 1000 1 1000 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done
	for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 1000 2 1000 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done
	for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 1000 4 1000 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done
	for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 1000 8 1000 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done
	for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 1000 16 1000 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done

	# Fence pointer of length 1250
	#for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 100000 1 1250 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done
	#for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 100000 2 1250 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done
	#for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 100000 4 1250 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done
	#for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 100000 8 1250 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done
	#for i in $(seq 1 20); do upbit_fence_test ub update $2 4 ${UPBIT}/${1} 100000 100000 16 1250 0 | tee /dev/tty | tail -n1 >> $FL_TEST_RESULTS; done
}

# Main execution sequence
if [ "$3" == "thread" ]; then
	run_thread_test $1 $2
elif [ "$3" == "appr" ]; then
	run_appr_test $1 $2
elif [ "$3" == "fence-length" ]; then
	run_fence_length_test $1 $2
else
	# 1 - the data set
	# 2 - the number of rows
	run_thread_test $1 $2
	run_appr_test $1 $2
	run_fence_length_test $1 $2
fi
